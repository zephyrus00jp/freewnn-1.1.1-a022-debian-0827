Source: freewnn
Section: utils
Priority: optional
Maintainer: Keita Maehara <maehara@debian.org>
Uploaders: Hideki Yamane <henrich@debian.org>
Build-Depends: debhelper (>= 7.0.50~), libncurses5-dev, autoconf, automake, libtool
Standards-Version: 3.9.2
Homepage: http://sourceforge.jp/projects/freewnn/

Package: freewnn-common
Architecture: all
Depends: ${misc:Depends}
Conflicts: wnn-common, freewnn-jserver (<< 1.1.0+1.1.1-a017-4)
Replaces: wnn-common
Description: Files shared among the FreeWnn packages
 FreeWnn is a network-extensible Japanese/Chinese/Korean input
 system.  It was jointly developed and released by the Software
 Research Group of Kyoto University Research Institute for
 Mathematical Science, OMRON Corporation and Astec, Inc. and now
 maintained by the FreeWnn Project.
 .
 This package contains files that are shared among the FreeWnn
 packages.

Package: freewnn-jserver
Architecture: any
Depends: freewnn-common, adduser (>= 3.34), ${shlibs:Depends}, ${misc:Depends}
Conflicts: wnn
Replaces: wnn
Description: Japanese input system
 FreeWnn jserver (Wnn) is a network-extensible Kana-to-Kanji
 conversion system.  It was jointly developed and released by the
 Software Research Group of Kyoto University Research Institute for
 Mathematical Science, OMRON Corporation and Astec, Inc. and now
 maintained by the FreeWnn Project.

Package: freewnn-cserver
Architecture: any
Depends: freewnn-common, adduser (>= 3.34), ${shlibs:Depends}, ${misc:Depends}
Conflicts: cwnn
Replaces: cwnn
Description: Chinese input system
 FreeWnn cserver (cWnn) is an integrated Chinese input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Chinese users from all over the world,
 including P.R.China and Taiwan. FreeWnn cserver is capable of
 carrying out Hanzi conversion from an arbitrary Pinyin or Zhuyin
 sequence, hence improving the speed of Pinyin/Zhuyin input.

Package: freewnn-kserver
Architecture: any
Depends: freewnn-common, adduser (>= 3.34), ${shlibs:Depends}, ${misc:Depends}
Conflicts: kwnn
Replaces: kwnn
Description: Korean input system
 FreeWnn kserver (kWnn) is an integrated Korean input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Korean users from all over the world.

Package: libwnn0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: FreeWnn library for Wnn (FreeWnn jserver)
 FreeWnn jserver (Wnn) is a network-extensible Kana-to-Kanji
 conversion system.  It was jointly developed and released by the
 Software Research Group of Kyoto University Research Institute for
 Mathematical Science, OMRON Corporation and Astec, Inc. and now
 maintained by the FreeWnn Project.
 .
 This package contains the dynamic libraries for Wnn.

Package: libcwnn0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: FreeWnn library for cWnn (FreeWnn cserver)
 FreeWnn cserver (cWnn) is an integrated Chinese input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Chinese users from all over the world,
 including P.R.China and Taiwan. FreeWnn cserver is capable of
 carrying out Hanzi conversion from an arbitrary Pinyin or Zhuyin
 sequence, hence improving the speed of Pinyin/Zhuyin input.
 .
 This package contains the dynamic libraries for cWnn.

Package: libkwnn0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: FreeWnn library for kWnn (FreeWnn kserver)
 FreeWnn kserver (kWnn) is an integrated Korean input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Korean users from all over the world.
 .
 This package contains the dynamic libraries for kWnn.

Package: libwnn-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: wnn6-dev, wnn-dev, freewnn-jserver-dev
Replaces: wnn-dev, freewnn-jserver-dev
Description: Header files and static libraries for Wnn (FreeWnn jserver)
 FreeWnn jserver (Wnn) is a network-extensible Kana-to-Kanji
 conversion system.  It was jointly developed and released by the
 Software Research Group of Kyoto University Research Institute for
 Mathematical Science, OMRON Corporation and Astec, Inc. and now
 maintained by the FreeWnn Project.
 .
 This package contains the header files and static library for
 Wnn. Install this package if you wish to develop your own Wnn client
 programs.

Package: libcwnn-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: cwnn-dev, freewnn-cserver-dev
Replaces: cwnn-dev, freewnn-cserver-dev
Description: Header files and static library for cWnn (FreeWnn cserver)
 FreeWnn cserver (cWnn) is an integrated Chinese input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Chinese users from all over the world,
 including P.R.China and Taiwan. FreeWnn cserver is capable of
 carrying out Hanzi conversion from an arbitrary Pinyin or Zhuyin
 sequence, hence improving the speed of Pinyin/Zhuyin input.
 .
 This package contains the header files and the static library for
 cWnn. Install this package if you wish to develop your own cWnn
 client programs.

Package: libkwnn-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: kwnn-dev, freewnn-kserver-dev
Replaces: kwnn-dev, freewnn-kserver-dev
Description: Header files and static library for kWnn (FreeWnn kserver)
 FreeWnn kserver (kWnn) is an integrated Korean input system running
 on Unix workstation. It supports a wide range of input methods,
 satisfying the needs of the Korean users from all over the world.
 .
 This package contains the header files and the static library for
 kWnn. Install this package if you wish to develop your own kWnn
 client programs.

#Package: uum
#Section: utils
#Architecture: any
#Depends: ${shlibs:Depends}
#Conflicts: freewnn-jserver (<< 1.1.0+1.1.1-a019-1)
#Description: Kana-To-Kanji conversion front-end processor for FreeWnn jserver
# Uum is a command line utility which provides Japanese input/output
# environment on the terminal. Uum is able to connect with FreeWnn
# jserver of other machine via the internet.

