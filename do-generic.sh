:
#
make distclean
# env CC="gcc-4.9" 
env CCOPTIONS="-Werror -Wall -Dlinux -D_GNU_SOURCE -D_DEFAULT_SOURCE" ./configure --prefix=/usr --mandir=/usr/share/man --enable-client=yes  --disable-traditional-layout
env LANG=C LC_ALL=C make CCOPTIONS="-Werror -Wall -D_GNU_SOURCE -D_DEFAULT_SOURCE"
